﻿using NUnit.Framework;

namespace com.br.k21.SistemaVendas.Testes
{
    public class TestesCalculadoraComissao
    {
        [Test]
        public void Venda_de_1000_retorna_comissao_de_50()
        {
            decimal comissaoEsperada = 50.0M;
            decimal totalVendas = 1000.0M;
            CalculadoraComissao calculo = new CalculadoraComissao();
            decimal comissao = calculo.Calcular(totalVendas);

            Assert.AreEqual(comissaoEsperada, comissao);
        }

        [Test]
        public void Venda_de_2000_retorna_comissao_de_100()
        {
            decimal totalVendas = 2000.0M;
            CalculadoraComissao calculo = new CalculadoraComissao();
            decimal comissao = calculo.Calcular(totalVendas);

            Assert.AreEqual(100.0M, comissao);
        }

        [Test]
        public void Venda_de_10000_retorna_comissao_de_600()
        {
            decimal totalVendas = 10000;
            CalculadoraComissao calculo = new CalculadoraComissao();
            decimal comissao = calculo.Calcular(totalVendas);

            Assert.AreEqual(600, comissao);
        }

        [Test]
        public void Venda_de_77_99_retorna_comissao_de_3_89()
        {
            decimal totalVendas = 77.99M;
            CalculadoraComissao calculo = new CalculadoraComissao();
            decimal comissao = calculo.Calcular(totalVendas);

            Assert.AreEqual(3.89M, comissao);
        }

        [Test]
        public void Venda_de_1001_retorna_comissao_de_50_05()
        {
            decimal totalVendas = 1001;
            CalculadoraComissao calculo = new CalculadoraComissao();
            decimal comissao = calculo.Calcular(totalVendas);

            Assert.AreEqual(50.05M, comissao);
        }

        [Test]
        public void Venda_de_1001_nao_retorna_comissao_de_50_049999999999()
        {
            decimal totalVendas = 1001;
            CalculadoraComissao calculo = new CalculadoraComissao();
            decimal comissao = calculo.Calcular(totalVendas);

            Assert.AreNotEqual(50.049999999999M, comissao);
        }
    }
}
