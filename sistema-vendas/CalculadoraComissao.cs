﻿using System;

namespace com.br.k21.SistemaVendas
{
    public class CalculadoraComissao
    {
        public decimal Calcular(decimal p)
        {
            decimal comissao;

            if (p >= 0 && p < 10000)
            {
                comissao = p * 0.05M;
            }
            else
            {
                comissao = p * 0.06M;
            }

            return Math.Floor(comissao * 100) / 100;
        }
    }
}
